import React from 'react';
import { View } from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  const logos = {
      logo1: require('./logo.png'),
      logo2: require('./logo-2.png'),
    };

  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center"}}>
        <Card displayText="Hello1" logo={logos.logo1}/>
        <Card displayText="Hello2" logo={logos.logo2}/>
    </View>
  );
}

export default Inft2508App;