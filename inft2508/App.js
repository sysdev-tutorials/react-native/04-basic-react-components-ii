import React, {useState} from 'react';
import { 
  View, 
  TextInput, 
  ScrollView, 
  SafeAreaView,
} from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  const logos = {
      logo_music: require('./images/logo-music.png'),
      logo_vechiles: require('./images/logo-vechiles.png'),
      logo_food: require('./images/logo-food.png'),
      logo_realstate: require('./images/logo-realstate.jpeg'),
      logo_dating: require('./images/logo-dating.png'),
      logo_mobile: require('./images/logo-mobile.png'),
    };

    // initial state
    const initialState = [
      {id: 1, displayText: "Music", logo: logos.logo_music},
      {id: 2, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 3, displayText: "Food", logo: logos.logo_food},
      {id: 4, displayText: "Real State", logo: logos.logo_realstate},
      {id: 5, displayText: "Dating", logo: logos.logo_dating},
      {id: 6, displayText: "Mobile", logo: logos.logo_mobile},
      {id: 7, displayText: "Music", logo: logos.logo_music},
      {id: 8, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 9, displayText: "Food", logo: logos.logo_food},
      {id: 10, displayText: "Real State", logo: logos.logo_realstate},
      {id: 11, displayText: "Dating", logo: logos.logo_dating},
      {id: 12, displayText: "Mobile", logo: logos.logo_mobile},
      {id: 13, displayText: "Music", logo: logos.logo_music},
      {id: 14, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 15, displayText: "Food", logo: logos.logo_food},
      {id: 16, displayText: "Real State", logo: logos.logo_realstate},
      {id: 17, displayText: "Dating", logo: logos.logo_dating},
      {id: 18, displayText: "Mobile", logo: logos.logo_mobile},
    ]

    // set initial state
    const [cards, setCards] = useState (initialState);

  return (
    <SafeAreaView>
      <View style= {{ margin: 10, alignItems: "center" }}>
        <TextInput
          placeholder="Type here to search!"
          onChangeText={newText => {
            var matchedCards = [];
            // apply search only if search string length >= 2
            if(newText.trim().length >= 2) {
              initialState.map((value, index) => {
                if(value.displayText.includes(newText)) {
                  matchedCards.push(value);
                }
              });
              setCards(matchedCards);
            } else{
              setCards(initialState);
            }
          }}
        />
      </View>
      <ScrollView>
        <View style={{ 
            flex: 1, 
            justifyContent: "center", 
            alignItems: "center",
            flexDirection: "row",
            flexWrap: "wrap",
            alignContent: "center",
            }}>

            {
              cards.map((value, index) => {
                  return <Card key={value.id} displayText={value.displayText} logo={value.logo}/>
                }
              )
            }
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

export default Inft2508App;