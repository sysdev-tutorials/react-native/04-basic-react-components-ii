# 04 Basic React Components - II

In this lesson we will mainily focus on two themes.
- writing custom and resuable components,
- Working with application data using properties (props) and states

While learning new stuffs, we will continue improving the same app we have started in previous lesson.


## Preparations
We will copy inft2508 folder from the previous lession: https://gitlab.com/sysdev-tutorials/react-native/03-basic-react-components-i/-/tree/main/inft2508

Then we will create two folders inside it:
- 'components' folder: this folder will contain all reusable compopnents that will be developed
- 'images' folder: this folder will contain the images used in our app

Then copy the 'inft2508/logo.png' file to 'inft2508/images/logo.png'. And update the 'Image' reference in 'App.js' file.

<img align="right" src="state_prev_lesson.png"/>

```
import React from 'react';
import { Text, View, Image } from 'react-native';

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center"}}>
      <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10,
          justifyContent: "center", 
          alignItems: "center",
        }}>
        <View style= {{flex: 1, justifyContent: "flex-end"}}>
          <Image
            style= {{width: 20, height: 20}}
            source={require("./images/logo.png")}
          />
        </View>
        <View style= {{flex: 1, justifyContent: "flex-start"}}>
          <Text>Hello!</Text>
        </View>
      </View>
    </View>
  );
}

export default Inft2508App;
```
<br clear="both"/>

So, now we are at the state where we left in previous lesson. Lets then see how we can create reusable components!

## Writing custom and reusable component
In previous lession we have seen that we have to copy the inner view (child to the outer most view) multiple time if we want to have multiple 'Card' like blocks to be displayed! We can improve that by carving out that inner view block (and its children) into a seperate component, and then reuse that whereever needed. Lets name this new component 'Card.js' and place inside './inft2508/components' folder. 

Then use (reuse multiple times) 'Card.js' component into 'App.js' component. Like in the code snippet below! Inspect that we get exactly same result as before!

<img align="right" src="reusable_card_1.png"/>

```
// App.js file contents
import React from 'react';
import { View } from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center"}}>
        <Card/>
        <Card/>
        <Card/>
        <Card/>
    </View>
  );
}
export default Inft2508App;


// components/Card.js file contents
import React from 'react';
import { Text, View, Image } from 'react-native';

const Card = () => {
  return (
    <View style={{ 
      backgroundColor: "#9FE8FF",
      height: 100,
      width: 150,
      borderRadius: 10,
      justifyContent: "center", 
      alignItems: "center",
    }}>
    <View style= {{flex: 1, justifyContent: "flex-end"}}>
      <Image
        style= {{width: 20, height: 20}}
        source={require("../images/logo.png")}
      />
    </View>
    <View style= {{flex: 1, justifyContent: "flex-start"}}>
      <Text>Hello!</Text>
    </View>
  </View>
  );
}
export default Card;

```
<br clear="both"/>

## Tips on JSX
Going forward, we will often see JSX syntax - which is a syntax that lets you write html-ish elements inside JavaScript. It also allows JavaScript code or expression within html-ish elements. Note that JavaScript expression will be between curly braces​.

## Component properties
The 'Card' component we have designed is reusable but quite static and not customisable. Now, let´s try to customize it such that it can fit into different contexts. Specifically, let´s change its display text and the icon dynamically. One way of achieving this is via component properties (called props). The component properties are passed to the component when it is created and they are available to the entire lifecyle of it the component. Note however that the data passed via 'props' is ```fixed``` throughout the lifecycle of a component​.

So, let´s add and use properties to our 'Card' component as below.

Here, note that the properties ```displayText``` and ```logo``` with proper values are passed to the 'Card' component from the conparent 'Inft2508App' component in 'App.js' file. The 'Card' component is reused 6 times with proper logos and display texts.

Note also that ```margin: 10``` property is added to the 'Card' components root element so that we will have space around each 'Card', as shown in the figure below.

<img align="right" src="reusable_card_2.png"/>

```
// App.js file contents
import React from 'react';
import { View } from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  const logos = {
      logo_music: require('./images/logo-music.png'),
      logo_vechiles: require('./images/logo-vechiles.png'),
      logo_food: require('./images/logo-food.png'),
      logo_realstate: require('./images/logo-realstate.jpeg'),
      logo_dating: require('./images/logo-dating.png'),
      logo_mobile: require('./images/logo-mobile.png'),
    };

  return (
    <View style={{ 
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center",
        flexDirection: "row",
        flexWrap: "wrap",
        alignContent: "center",
        }}>
        <Card displayText="Music" logo={logos.logo_music}/>
        <Card displayText="Vechiles" logo={logos.logo_vechiles}/>
        <Card displayText="Food" logo={logos.logo_food}/>
        <Card displayText="Real State" logo={logos.logo_realstate}/>
        <Card displayText="Dating" logo={logos.logo_dating}/>
        <Card displayText="Mobile" logo={logos.logo_mobile}/>
    </View>
  );
}

export default Inft2508App;


// components/Card.js file contents
import React from 'react';
import { Text, View, Image } from 'react-native';

const Card = (props) => {
  return (
    <View style={{ 
          backgroundColor: "#9FE8FF",
          height: 100,
          width: 150,
          borderRadius: 10,
          justifyContent: "center", 
          alignItems: "center",
          margin: 10,
        }}>
        <View style= {{flex: 1, justifyContent: "flex-end"}}>
          <Image
            style= {{width: 20, height: 20}}
            source={props.logo}
          />
        </View>
        <View style= {{flex: 1, justifyContent: "flex-start"}}>
          <Text>{props.displayText}</Text>
        </View>
    </View>
  );
}

export default Card;

```
<br clear="both"/>

## Component state data
In the previous section we have used 'props' to dynamically customize a component. Note that the data passed via 'props' is ```fixed``` throughout the lifecycle of a component​.

If we need to work on a data that can over time through out the lifecycle of a component, we have to use another mechanism, called 'state'. It is like a component’s personal data storage and gives some memory to it.

In order to find some imaginary use-case in our application, we assume the following scenario.

### Scenario
> Assume that our app has several categories, each represented by a 'Card' component and all are displayed in the landing page. Since all of them will not fit into displayable part of the landing page, we want to have a 'search' field to search for category. Upon search, only matching 'Card' elements will be shown. So, the idea here is to store search result into component´s state - the main component´s or the 'Inft2508App' component in our case. That means the main component shall be rendered based on the data available in its state!

### Update UI
So, to start with, let´s add following elements to the main page
- a search box i.e ```TextInput``` element at the top. This is wrapped around a 'View' element for convinience which has some margin and centered cross-axis.
- a scrolling container ```ScrollView``` that wrapped the current 'View' element containing list of 'Card' components
- then add several more 'Card' components towards the end of already existing list
- finally all the elements are wrapped  within ```SafeAreaView```. The purpose of SafeAreaView is to render content within the safe area boundaries of a device. It is specially applicable to iOS devices. Note that error will be thrown if you 'return' multiple root elements from the ```return``` statement. So, all elements need to be wrapped into one container!

<img align="right" src="ui_with_scroll_search.png" width=250>

```
// contents of App.js file

import React from 'react';
import { 
  View, 
  TextInput, 
  ScrollView, 
  SafeAreaView 
} from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  const logos = {
      logo_music: require('./images/logo-music.png'),
      logo_vechiles: require('./images/logo-vechiles.png'),
      logo_food: require('./images/logo-food.png'),
      logo_realstate: require('./images/logo-realstate.jpeg'),
      logo_dating: require('./images/logo-dating.png'),
      logo_mobile: require('./images/logo-mobile.png'),
    };

  return (
    <SafeAreaView>
      <View style= {{ margin: 10, alignItems: "center" }}>
        <TextInput
          placeholder="Type here to search!"
        />
      </View>
    <ScrollView>
      <View style={{ 
          flex: 1, 
          justifyContent: "center", 
          alignItems: "center",
          flexDirection: "row",
          flexWrap: "wrap",
          alignContent: "center",
          }}>
          <Card displayText="Music" logo={logos.logo_music}/>
          <Card displayText="Vechiles" logo={logos.logo_vechiles}/>
          <Card displayText="Food" logo={logos.logo_food}/>
          <Card displayText="Real State" logo={logos.logo_realstate}/>
          <Card displayText="Dating" logo={logos.logo_dating}/>
          <Card displayText="Mobile" logo={logos.logo_mobile}/>

          <Card displayText="Music" logo={logos.logo_music}/>
          <Card displayText="Vechiles" logo={logos.logo_vechiles}/>
          <Card displayText="Food" logo={logos.logo_food}/>
          <Card displayText="Real State" logo={logos.logo_realstate}/>
          <Card displayText="Dating" logo={logos.logo_dating}/>
          <Card displayText="Mobile" logo={logos.logo_mobile}/>

          <Card displayText="Music" logo={logos.logo_music}/>
          <Card displayText="Vechiles" logo={logos.logo_vechiles}/>
          <Card displayText="Food" logo={logos.logo_food}/>
          <Card displayText="Real State" logo={logos.logo_realstate}/>
          <Card displayText="Dating" logo={logos.logo_dating}/>
          <Card displayText="Mobile" logo={logos.logo_mobile}/>
      </View>
    </ScrollView>
    </SafeAreaView>
  );
}

export default Inft2508App;
```

<br clear="both"/>

### Add 'state' data to a component

In order to solve above mentioned scenario using state, we will first initialize state with default values that will be the list of all available categories i.e we will display all corresponding 'Card' components. When something is typed on seach field, we will find relavant categories and update them to the component state. That means only those relavant will then be displayed on the screen.

>Note: This is just showing the use case of using states. In a real scenario, you will probably do search against a real data in a database and then accordingly update state and/or user interface.

Traditionally, 'state' in react native were available in class based components. From react 16.8, they are also available as using a mechanism called 'hooks'. Since we are using function based components so far, we will use 'state' using 'hooks'! You are encouraged to read more about hooks here https://reactjs.org/docs/hooks-intro.html.

In order to use 'state' hook, we need to do the following
 - First, import 'useState' function (we call it hook) from 'react' package
 - Then we call it (the useState function) inside a function component. This does two things
   - it takes some input data and add that to the component state
   - it returns a pair: the current variable (with updated value), and a function that lets you to update it. For example like this ```const [count, setCount] = useState(0)```. Here, ```count``` is a state variable whose value is set to ```0```, and ```setCount``` is a function to be used when updating the 'count' state variable.
  
Now, lets use state in our app.

First lets create a data structure that will be used to represent a card. List of cards will then be represented by such data structure and initialized as initial component state! This can be done in 3 steps

- Import from react package ```import React, {useState} from 'react';```
- Define data structure, 
  ```
  const initialState = [
      {id: 1, displayText: "Music", logo: logos.logo_music},
      {id: 2, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 3, displayText: "Food", logo: logos.logo_food},
      ...
      ]
  ```
- Then initialize the component state, like this 
  ```
  const [cards, setCards] = useState (initialState);
  ```

Then, ```cards``` state variable which is available in the component scope is used to render the UI, like this
```
  {
     cards.map((value, index) => {
        return <Card key={value.id} displayText={value.displayText} logo={value.logo}/>
      }
    )
  }  
```

Now lets see the complete App.js file. If you inspect, the effect will be same as before i.e. without using ```state```.



<img align="right" src="">

```
import React, {useState} from 'react';
import { 
  View, 
  TextInput, 
  ScrollView, 
  SafeAreaView 
} from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  const logos = {
      logo_music: require('./images/logo-music.png'),
      logo_vechiles: require('./images/logo-vechiles.png'),
      logo_food: require('./images/logo-food.png'),
      logo_realstate: require('./images/logo-realstate.jpeg'),
      logo_dating: require('./images/logo-dating.png'),
      logo_mobile: require('./images/logo-mobile.png'),
    };

    // initial state
    const initialState = [
      {id: 1, displayText: "Music", logo: logos.logo_music},
      {id: 2, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 3, displayText: "Food", logo: logos.logo_food},
      {id: 4, displayText: "Real State", logo: logos.logo_realstate},
      {id: 5, displayText: "Dating", logo: logos.logo_dating},
      {id: 6, displayText: "Mobile", logo: logos.logo_mobile},
      {id: 7, displayText: "Music", logo: logos.logo_music},
      {id: 8, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 9, displayText: "Food", logo: logos.logo_food},
      {id: 10, displayText: "Real State", logo: logos.logo_realstate},
      {id: 11, displayText: "Dating", logo: logos.logo_dating},
      {id: 12, displayText: "Mobile", logo: logos.logo_mobile},
      {id: 13, displayText: "Music", logo: logos.logo_music},
      {id: 14, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 15, displayText: "Food", logo: logos.logo_food},
      {id: 16, displayText: "Real State", logo: logos.logo_realstate},
      {id: 17, displayText: "Dating", logo: logos.logo_dating},
      {id: 18, displayText: "Mobile", logo: logos.logo_mobile},
    ]

    // set initial state
    const [cards, setCards] = useState (initialState);

  return (
    <SafeAreaView>
      <View style= {{ margin: 10, alignItems: "center" }}>
        <TextInput
          placeholder="Type here to search!"
        />
      </View>
    <ScrollView>
      <View style={{ 
          flex: 1, 
          justifyContent: "center", 
          alignItems: "center",
          flexDirection: "row",
          flexWrap: "wrap",
          alignContent: "center",
          }}>

          {
            cards.map((value, index) => {
                return <Card key={value.id} displayText={value.displayText} logo={value.logo}/>
              }
            )
          }  

      </View>
    </ScrollView>
    </SafeAreaView>
  );
}

export default Inft2508App;
```
<br clear="both"/>

### Changing state data and update UI automatically
We will do this by adding state change logic inside ``` onChangeText``` event handler function. The snippet of state change login is like below. For the sake of simplicity, the search filter will be applied is search string length is 2 or more!

```
  onChangeText={newText => {
    var matchedCards = [];
    // apply search only if search string is longer than 2 char
    if(newText.trim().length >= 2) {
      initialState.map((value, index) => {
        if(value.displayText.includes(newText)) {
          matchedCards.push(value);
        }
      });
      setCards(matchedCards);
    } else{
      setCards(initialState);
    }
  }}
```

Thats it. Whenever something is typed in the search ```TextInput``` field, component state will be updated and rerendered automatically. Since the UI is constructed using state data, we will see changes in the UI. The complete code for ```App.js``` file is below.

<img align="right" src="searchable_ui.png">

```
import React, {useState} from 'react';
import { 
  View, 
  TextInput, 
  ScrollView, 
  SafeAreaView 
} from 'react-native';
import Card from './components/Card'

const Inft2508App = () => {
  const logos = {
      logo_music: require('./images/logo-music.png'),
      logo_vechiles: require('./images/logo-vechiles.png'),
      logo_food: require('./images/logo-food.png'),
      logo_realstate: require('./images/logo-realstate.jpeg'),
      logo_dating: require('./images/logo-dating.png'),
      logo_mobile: require('./images/logo-mobile.png'),
    };

    // initial state
    const initialState = [
      {id: 1, displayText: "Music", logo: logos.logo_music},
      {id: 2, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 3, displayText: "Food", logo: logos.logo_food},
      {id: 4, displayText: "Real State", logo: logos.logo_realstate},
      {id: 5, displayText: "Dating", logo: logos.logo_dating},
      {id: 6, displayText: "Mobile", logo: logos.logo_mobile},
      {id: 7, displayText: "Music", logo: logos.logo_music},
      {id: 8, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 9, displayText: "Food", logo: logos.logo_food},
      {id: 10, displayText: "Real State", logo: logos.logo_realstate},
      {id: 11, displayText: "Dating", logo: logos.logo_dating},
      {id: 12, displayText: "Mobile", logo: logos.logo_mobile},
      {id: 13, displayText: "Music", logo: logos.logo_music},
      {id: 14, displayText: "Vechiles", logo: logos.logo_vechiles},
      {id: 15, displayText: "Food", logo: logos.logo_food},
      {id: 16, displayText: "Real State", logo: logos.logo_realstate},
      {id: 17, displayText: "Dating", logo: logos.logo_dating},
      {id: 18, displayText: "Mobile", logo: logos.logo_mobile},
    ]

    // set initial state
    const [cards, setCards] = useState (initialState);

  return (
    <SafeAreaView>
      <View style= {{ margin: 10, alignItems: "center" }}>
        <TextInput
          placeholder="Type here to search!"
          onChangeText={newText => {
            var matchedCards = [];
            // apply search only if search string is longer than 2 char
            if(newText.trim().length >= 2) {
              initialState.map((value, index) => {
                if(value.displayText.includes(newText)) {
                  matchedCards.push(value);
                }
              });
              setCards(matchedCards);
            } else{
              setCards(initialState);
            }
          }}
        />
      </View>
    <ScrollView>
      <View style={{ 
          flex: 1, 
          justifyContent: "center", 
          alignItems: "center",
          flexDirection: "row",
          flexWrap: "wrap",
          alignContent: "center",
          }}>

          {
            cards.map((value, index) => {
                return <Card key={value.id} displayText={value.displayText} logo={value.logo}/>
              }
            )
          }
      </View>
    </ScrollView>
    </SafeAreaView>
  );
}

export default Inft2508App;
```
<br clear="both"/>


## That´s for this lesson
Congratulations!! You have learned about writing reusable components, and about using component properties and state. Besides that we have also used couple of new react native components in this session such as ScrollView, SafeAreaView, etc.

In the next lesson, we will continue improving our app and at the same time learning new stuffs. Particularly next lesson will focus on
- working with 'touchable' components. For example we want each card displayed in the main screen 'touchable'!
- working with lists. For example when a particular card is clicked, we would like to show all the items under it!
- using third party component


